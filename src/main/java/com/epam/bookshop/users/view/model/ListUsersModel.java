package com.epam.bookshop.users.view.model;

import java.util.List;

public class ListUsersModel {
	private List<UserSummaryView> users;

	public ListUsersModel(List<UserSummaryView> users) {
		super();
		this.users = users;
	}

	public List<UserSummaryView> getUsers() {
		return users;
	}

}
