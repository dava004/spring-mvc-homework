package com.epam.bookshop.users.view.transform;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import com.epam.bookshop.user.domain.User;
import com.epam.bookshop.users.view.model.UserSummaryView;

@Component
public class UserTransformer {

	ConversionService conversionService;

	@Autowired
	public UserTransformer(ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	public List<UserSummaryView> transformUsers(List<User> users) {
		List<UserSummaryView> result = new ArrayList<UserSummaryView>();
		for (User user : users) {
			result.add(transformUser(user));
		}
		return result;
	}

	public UserSummaryView transformUser(User user) {
		UserSummaryView result = new UserSummaryView();
		result.setName(user.getName());
		result.setEmail(user.getEmail());
		result.setBirthDate(conversionService.convert(user.getBirthDate(),
				String.class));
		return result;
	}
}
