package com.epam.bookshop.users.view.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.bookshop.user.domain.User;
import com.epam.bookshop.user.service.UserSearchService;
import com.epam.bookshop.users.view.model.ListUsersModel;
import com.epam.bookshop.users.view.model.ListUsersRequest;
import com.epam.bookshop.users.view.model.UserSummaryView;
import com.epam.bookshop.users.view.transform.UserTransformer;

@Controller
public class ListUsersController {
    private UserSearchService userSearchService;
    private UserTransformer userTransformer;

    @Autowired
    public ListUsersController(UserSearchService userSearchService, UserTransformer userTransformer) {
        super();
        this.userSearchService = userSearchService;
        this.userTransformer = userTransformer;
    }

    @ModelAttribute("listUsersModel")
    public ListUsersModel createListUsersModel(ListUsersRequest listUserRequest) {
        List<User> users = listUsers(listUserRequest);
        List<UserSummaryView> userViews = transformUsers(users);
        return initListUsersModel(userViews);
    }

    @RequestMapping("/users.html")
    public String showUsers() {
        return "users";
    }

    private List<UserSummaryView> transformUsers(List<User> users) {
        return userTransformer.transformUsers(users);
    }

    private ListUsersModel initListUsersModel(List<UserSummaryView> userViews) {
        return new ListUsersModel(userViews);
    }

    private List<User> listUsers(ListUsersRequest searchUsersRequest) {
        return userSearchService.listUsers(searchUsersRequest.getSearchQuery());
    }

}
