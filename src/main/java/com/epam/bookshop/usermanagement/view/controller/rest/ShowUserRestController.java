package com.epam.bookshop.usermanagement.view.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.epam.bookshop.user.service.UserSearchService;
import com.epam.bookshop.users.view.model.UserSummaryView;
import com.epam.bookshop.users.view.transform.UserTransformer;

@RestController
@RequestMapping("/rest-api")
public class ShowUserRestController {
	private UserSearchService userSearchService;
	private UserTransformer userTransformer;

	@Autowired
	public ShowUserRestController(UserSearchService userSearchService,
			UserTransformer userTransformer) {
		super();
		this.userSearchService = userSearchService;
		this.userTransformer = userTransformer;
	}

	@RequestMapping("users/{userId}")
	public UserSummaryView getUser(@PathVariable Long userId) {
		return userTransformer
				.transformUser(userSearchService.findUser(userId));
	}
}
