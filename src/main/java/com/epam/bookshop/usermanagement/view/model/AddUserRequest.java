package com.epam.bookshop.usermanagement.view.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class AddUserRequest {
	@NotNull
	@Size(min = 2, max = 255)
	private String name;
	@NotNull
	@Size(min = 2, max = 255)
	private String password;
	@NotNull
	@Size(min = 2, max = 255)
	private String email;
	@NotNull
	@DateTimeFormat(pattern="YYYY.MM.dd")
	private Date birthDate;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
