package com.epam.bookshop.usermanagement.view.transform;

import org.springframework.stereotype.Component;

import com.epam.bookshop.user.domain.User;
import com.epam.bookshop.usermanagement.view.model.AddUserRequest;

@Component
public class AddUserRequestTransformer {
	public User transformAddUserRequestToUser(AddUserRequest addUserRequest) {
		User result = new User();
		result.setName(addUserRequest.getName());
		result.setPassword(addUserRequest.getPassword());
		result.setEmail(addUserRequest.getEmail());
		result.setBirthDate(addUserRequest.getBirthDate());
		return result;
	}
}