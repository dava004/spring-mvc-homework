package com.epam.bookshop.usermanagement.view.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.bookshop.usermanagement.view.model.AddUserRequest;

@Controller
public class AddUserFormController {

	@ModelAttribute("addUserRequest")
	public AddUserRequest createListBooksModel(@ModelAttribute AddUserRequest addUserRequest) {
		return new AddUserRequest();
	}

	@RequestMapping(value = "/addUserForm.html", method = RequestMethod.GET)
	private String createUser() {
		return "add_user";
	}

}
