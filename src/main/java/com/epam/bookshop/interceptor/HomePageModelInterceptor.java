package com.epam.bookshop.interceptor;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.epam.bookshop.authentication.service.AuthenticationService;
import com.epam.bookshop.authentication.view.controller.LoginFormController;
import com.epam.bookshop.home.view.model.HomepageModel;
import com.epam.bookshop.home.view.model.LanguageUrlMapping;
import com.epam.bookshop.home.view.support.LocalizationUrlBuilder;
import com.epam.bookshop.i18n.service.LocalizationService;
import com.epam.bookshop.stock.view.controller.AddBookFormController;

public class HomePageModelInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private LocalizationService localizationService;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private LocalizationUrlBuilder localizationUrlBuilder;

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		modelAndView.addObject("homepageModel", generateHomepageModel());
	}

	private HomepageModel generateHomepageModel() {
		HomepageModel result = new HomepageModel();
		result.setBookshopName("Bookshop");
		result.setLanguageSelectors(getLanguageSelectors());
		if (authenticationService.isUserAuthenticated()) {
			result.setLogoutUrl("/j_spring_security_logout");
			if (authenticationService.isUserAdmin()) {
				result.setAdminUrl(AddBookFormController.REQUEST_MAPPING);
			}
		} else {
			result.setLoginUrl(LoginFormController.REQUEST_MAPPING);
		}
		return result;
	}

	private List<LanguageUrlMapping> getLanguageSelectors() {
		return localizationUrlBuilder
				.buildAccessibleLanguageSelectors(localizationService
						.getAccessibleLanguages());
	}

}
