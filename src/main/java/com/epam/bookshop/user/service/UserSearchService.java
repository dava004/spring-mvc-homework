package com.epam.bookshop.user.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.bookshop.user.domain.User;
import com.epam.bookshop.user.repository.dao.UserDao;
import com.epam.bookshop.user.repository.domain.UserEntity;
import com.epam.bookshop.user.service.transform.UserEntityTransformer;

@Service
public class UserSearchService {
	private UserDao userDao;
	private UserEntityTransformer userEntityTransformer;

	@Autowired
	public UserSearchService(UserDao userDao,
			UserEntityTransformer userEntityTransformer) {
		super();
		this.userDao = userDao;
		this.userEntityTransformer = userEntityTransformer;
	}

	public List<User> listUsers() {
		return transformUserEntities(findUserEntities());
	}

	private List<User> transformUserEntities(Iterable<UserEntity> Users) {
		return userEntityTransformer.transformUserEntities(Users);
	}

	private Iterable<UserEntity> findUserEntities() {
		return userDao.findAll();
	}
	
    public User findUser(Long userId) {
        return transformUserEntity(doFindUser(userId));
    }
    
    private User transformUserEntity(UserEntity user) {
        return userEntityTransformer.transformUserEntity(user);
    }
    
    private UserEntity doFindUser(Long userId) {
        return userDao.findOne(userId);
    }

	public List<User> listAllUsers() {
		return transformUserEntities(userDao.findAll());
	}

	public List<User> listUsers(String searchQuery) {
		return transformUserEntities(findUserEntities(formatQuery(searchQuery)));
	}

	private String formatQuery(String searchQuery) {
		String result;
		if (searchQuery == null) {
			result = "%";
		} else {
			result = String.format("%%%s%%", searchQuery);
		}
		return result;
	}

	private Iterable<UserEntity> findUserEntities(String query) {
		Iterable<UserEntity> name = userDao.findByNameIgnoreCaseLike(query);
		Iterable<UserEntity> email = userDao.findByEmailIgnoreCaseLike(query);
		Set<UserEntity> results = new HashSet<>();
		results.addAll(convertIterableToSet(name));
		results.addAll(convertIterableToSet(email));
		return results;
	}

	private <T> Set<T> convertIterableToSet(Iterable<T> iterable) {
		Set<T> resultSet = new HashSet<>();
		for (T t : iterable) {
			resultSet.add(t);
		}
		return resultSet;
	}

}
