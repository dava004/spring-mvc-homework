package com.epam.bookshop.user.view.formatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class UserDateFormatter implements Formatter<Date> {

	@Override
	public String print(Date date, Locale locale) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
		return dateFormat.format(date);
	}

	@Override
	public Date parse(String text, Locale locale) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
		return dateFormat.parse(text);
	}

}
