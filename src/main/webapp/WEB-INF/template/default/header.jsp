<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="headerText" />
<div id="header">
	<div class="authentication">
		<c:if test="${not empty homepageModel.loginUrl}">
			<a href="<c:url value='${homepageModel.loginUrl}' />">Login</a>
		</c:if>
		<c:if test="${not empty homepageModel.logoutUrl}">
			<a href="<c:url value='${homepageModel.logoutUrl}' />">Logout</a>
		</c:if>
	</div>
	<h1>
		<tiles:insertAttribute name="headerText" />
	</h1>
</div>