<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<spring:theme code='style'/>"
	type="text/css" media="all">
<title><tiles:insertAttribute name="title" /></title>
</head>
<body>
	<div id="layout-one-fixed">
		<div id="container">
			<tiles:insertAttribute name="header" />
			<tiles:insertAttribute name="navigation" />
			<div id="content">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
	</div>
</body>
</html>