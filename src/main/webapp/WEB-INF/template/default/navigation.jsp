<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="navigation">
	<ul>
		<li><a href="<c:url value='/' />">Home</a></li>
		<li><a href="<c:url value='/shopping.html' />">Bookstore</a></li>
		<li><a href="<c:url value='/addUserForm.html' />">Registration</a></li>
		<c:if test="${not empty homepageModel.adminUrl}">
			<li><a href="<c:url value='${homepageModel.adminUrl}' />">Manage
					books</a></li>
			<li><a href="<c:url value='/users.html' />">Users</a></li>
		</c:if>
		<li><a href="<c:url value='/showShoppingCart.html' />">View
				shopping cart</a></li>
		<li>
			<div class="language-selector">
				<c:forEach var="languageSelector"
					items="${homepageModel.languageSelectors}">
					<a href="<c:url value='${languageSelector.languageSelectorUrl}' />">${languageSelector.languageCode}</a>
				</c:forEach>
			</div>
		</li>
	</ul>
</div>