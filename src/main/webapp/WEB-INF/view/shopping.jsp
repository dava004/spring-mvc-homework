<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<tiles:insertDefinition name="shopping">
	<tiles:putAttribute name="body">
		<form:form modelAttribute="listBooksRequest" method="get">
			<label for="title">Title</label>
			<form:input path="title" type="text" id="title" placeholder="Title" />
			<button type="submit">Search</button>
		</form:form>
		<table>
			<tr>
				<th>Title</th>
				<th>Author</th>
				<th>Available format</th>
			</tr>
			<c:forEach var="book" items="${listBooksModel.books}">
				<tr>
					<td><a href="<c:url value='${book.detailsUrl}' />">${book.title}</a></td>
					<td>${book.author}</td>
					<td>${book.bookFormat}</td>
				</tr>
			</c:forEach>
		</table>
	</tiles:putAttribute>
</tiles:insertDefinition>