<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="shopping_cart">
	<tiles:putAttribute name="body">
		<c:choose>
			<c:when test="${not empty shoppingCartModel.cartItems}">
				<table>
					<tr>
						<th>Title</th>
						<th>Author</th>
						<th>Format</th>
						<th>Quantity</th>
					</tr>
					<c:forEach var="cartItem" items="${shoppingCartModel.cartItems}">
						<tr>
							<td><a href="<c:url value='${cartItem.book.detailsUrl}' />">${cartItem.book.title}</a></td>
							<td>${cartItem.book.author}</td>
							<td>${cartItem.book.bookFormat}</td>
							<td>${cartItem.quantity}</td>
						</tr>
					</c:forEach>
				</table>
				<a
					href="<c:url value='${shoppingCartModel.clearShoppingCartUrl}' />">Empty
					cart</a>
			</c:when>
			<c:otherwise>
				<p>Your shopping cart is empty.</p>
			</c:otherwise>
		</c:choose>
	</tiles:putAttribute>
</tiles:insertDefinition>
