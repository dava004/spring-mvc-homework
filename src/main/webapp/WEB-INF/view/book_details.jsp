<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="book_details">
	<tiles:putAttribute name="body">
		<h2>Title</h2>
		<p>${bookDetailsModel.bookSummary.title}</p>
		<h2>Author</h2>
		<p>${bookDetailsModel.bookSummary.author}</p>
		<h2>Synopsis</h2>
		<p>${bookDetailsModel.bookDetails.synopsis}</p>
		<p>
			<form:form modelAttribute="addShoppingCartBookItemRequest"
				servletRelativeAction="${bookDetailsModel.addToCartUrl}">
				<form:hidden path="bookId" />
				<button type="submit">Add to cart</button>
			</form:form>
		</p>
		<c:if test="${not empty bookDetailsModel.bookDetails.coverUrl}">
			<h2>Cover</h2>
			<img src="<c:url value='${bookDetailsModel.bookDetails.coverUrl}' />" />
		</c:if>
	</tiles:putAttribute>
</tiles:insertDefinition>

