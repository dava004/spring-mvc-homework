<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<tiles:insertDefinition name="add_user">
	<tiles:putAttribute name="body">
		<c:if test="${not empty message}">
			<div class="alert alert-success">${message}</div>
		</c:if>
		<form:form modelAttribute="addUserRequest" action="addUserPost.html">
			<div class="form-group">
				<p>
					<label for="input_username">Username</label>
					<form:input path="name" id="input_username" placeholder="Username" />
					<form:errors path="name" element="div" cssClass="validation-error" />
				</p>
				<p>
					<label for="input_email">Email</label>
					<form:input path="email" id="input_email" placeholder="Email" />
					<form:errors path="email" element="div" cssClass="validation-error" />
				</p>
				<p>
					<label for="input_password">Password</label>
					<form:password path="password" id="input_password"
						placeholder="Password" />
					<form:errors path="password" element="div"
						cssClass="validation-error" />
				</p>
				<p>
					<label for="input_birthdate">Birthdate</label>
					<form:input path="birthDate" id="input_birthdate"
						placeholder="Birthdate" />

					<form:errors path="birthDate" element="div"
						cssClass="validation-error" />
				</p>
				<p>
					<button type="submit" class="btn btn-default">Add</button>
				</p>
			</div>
		</form:form>
	</tiles:putAttribute>
</tiles:insertDefinition>
