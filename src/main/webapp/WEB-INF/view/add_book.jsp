<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="add_book">
	<tiles:putAttribute name="body">
		<c:if test="${not empty message}">
			<div class="alert alert-success">${message}</div>
		</c:if>
		<form:form modelAttribute="addBookRequest" action="addBookPost.html"
			enctype="multipart/form-data">
			<div class="form-group">
				<p>
					<label for="input_title">Title</label>
					<form:input path="title" id="input_title" placeholder="Title" />
					<form:errors path="title" element="div" cssClass="validation-error" />
				</p>
				<p>
					<label for="input_author">Author</label>
					<form:input path="author" id="input_author" placeholder="Author" />
					<form:errors path="author" element="div"
						cssClass="validation-error" />
				</p>
				<p>
					<label for="input_format">Format</label>
					<form:select path="format" id="input_format"
						items="${addBookFormModel.availableBookFormats}" />
				</p>
				<p>
					<label for="input_synopsis">Synopsis</label>
					<form:textarea path="synopsis" id="input_synopsis"
						placeholder="Synopsis" />
				</p>
				<p>
					<label for="cover">Cover</label>
					<form:input path="cover" type="file" id="cover" />
				</p>
				<%-- 						<input type="hidden" name="${_csrf.parameterName}" --%>
				<%-- 							value="${_csrf.token}" /> --%>

			</div>

			<p>
				<button type="submit" class="btn btn-default">Add</button>
			</p>
		</form:form>
	</tiles:putAttribute>
</tiles:insertDefinition>
