<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<tiles:insertDefinition name="homepage">
	<tiles:putAttribute name="body">
		<p>
			<spring:message code="com.epam.bookshop.welcome_message" />
		</p>
	</tiles:putAttribute>
</tiles:insertDefinition>