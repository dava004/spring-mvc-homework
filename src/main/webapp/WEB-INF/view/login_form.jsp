<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="login">
	<tiles:putAttribute name="body">
		<form:form modelAttribute="loginRequest"
			action="j_spring_security_check">
			<form:errors element="div" cssClass="validation-error" />
			<div class="form-group">
				<p>
					<label for="input_username">Username</label>
					<form:input path="username" id="input_username"
						placeholder="Username" />
				</p>
				<p>
					<label for="input_password">Password</label>
					<form:password path="password" id="input_password"
						placeholder="Password" />
				</p>
			</div>
			<p>
				<button type="submit" class="btn btn-default">Login</button>
			</p>
		</form:form>
	</tiles:putAttribute>
</tiles:insertDefinition>