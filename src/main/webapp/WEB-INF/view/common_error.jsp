<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<p class="commmon-error">Something went wrong, please try again!</p>
	</tiles:putAttribute>
</tiles:insertDefinition>