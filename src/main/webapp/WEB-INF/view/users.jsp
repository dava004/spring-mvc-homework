<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<tiles:insertDefinition name="users">
	<tiles:putAttribute name="body">
		<form action="<c:url value='/users.html' />" method="get">
			<label for="searchQuery">Search</label> <input type="text"
				id="searachQuery" name="searchQuery"
				value="${listUsersRequest.searchQuery}" />
			<button type="submit">Search</button>
		</form>
		<table>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Birth date</th>
			</tr>

			<c:forEach var="user" items="${listUsersModel.users}">
				<tr>
					<td>${user.name}</td>
					<td>${user.email}</td>
					<td>${user.birthDate}</td>
				</tr>
			</c:forEach>

		</table>
	</tiles:putAttribute>
</tiles:insertDefinition>